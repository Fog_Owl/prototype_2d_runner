﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManagerScript : MonoBehaviour
{
    public Text Score;
    public Text HighScore;

    public Text Stars;
    private int starsBalance;

    public float scoreCount;
    public float HighScoreCount;

    public float pointsPerSecond;
    public bool scoreIncreasing;

    private void Start()
    {
        starsBalance = PlayerPrefs.GetInt("starsBalance");
        if(PlayerPrefs.GetFloat("HighScore") != 0)
            HighScoreCount = PlayerPrefs.GetFloat("HighScore");

    }


    private void Update()
    {
        if (scoreIncreasing)
            scoreCount += pointsPerSecond * Time.deltaTime;

        if (scoreCount > HighScoreCount)
        {
            HighScoreCount = scoreCount;
            PlayerPrefs.SetFloat("HighScore", HighScoreCount);
        }

        Stars.text = "Your balance : " + starsBalance;
        Score.text = "Score : " + Mathf.Round(scoreCount);
        HighScore.text = "Score : " + Mathf.Round(HighScoreCount);
    }

    public void AddScore(int pointToScore)
    {
        scoreCount += pointToScore;
    }

    public void TakeStars()
    {
        starsBalance++;
        PlayerPrefs.SetInt("starsBalance", starsBalance);
    }
}
