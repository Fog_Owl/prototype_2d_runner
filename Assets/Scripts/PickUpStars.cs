﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpStars : MonoBehaviour
{
    public int scoreToGive;

    private ScoreManagerScript ScoreManager;
    

    private void Start()
    {
        
        ScoreManager = FindObjectOfType<ScoreManagerScript>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.name == "Player")
        {
            ScoreManager.TakeStars();
            ScoreManager.AddScore(scoreToGive);
            Destroy(gameObject);
        }
    }


}
