﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float MoveSpeed;
    public float JumpForce;
    public float StaticMoveSpeed;
    public float SpeedMultiplayer;

    public GameObject DieScreen;



    Rigidbody2D rb;

    public bool IsGrounded;
    public LayerMask WhatIsGround;
    public Transform groundCheck;
    public float groundCheckRadius;

    Animator anim;

    public float JumpTime;
    private float JumpTimeCounter;

    public float speedIncreaseMilestone;
    private float speedMilestoneCount;

    public GameManager gameManager;

    public GameObject Shield;
    public bool isShield = false;

    

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();

        StaticMoveSpeed = MoveSpeed;

        JumpTimeCounter = JumpTime;

        speedMilestoneCount = speedIncreaseMilestone;
    }

    void Update()
    {

        IsGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, WhatIsGround);

        if (transform.position.x > speedMilestoneCount)
        {
            speedMilestoneCount += speedIncreaseMilestone;

            speedIncreaseMilestone = speedIncreaseMilestone * SpeedMultiplayer;
            StaticMoveSpeed *=  SpeedMultiplayer;
        }

        if (Input.GetAxis("Horizontal") > 0)
        {
            MoveSpeed = StaticMoveSpeed + 5f;
        }
        else if (Input.GetAxis("Horizontal") < 0)
        {
            MoveSpeed = StaticMoveSpeed - 3f;
        }
        else 
            MoveSpeed = StaticMoveSpeed;


        rb.velocity = new Vector2(MoveSpeed, rb.velocity.y);

        if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
        {
            
            if (IsGrounded)
            {
                rb.velocity = new Vector2(rb.velocity.x, JumpForce);
            }
            anim.SetBool("Grounded", IsGrounded);
        }
        
        if (Input.GetKey (KeyCode.Space) || Input.GetMouseButtonDown(0))
        {

            if (  JumpTimeCounter > 0  )
            {
                    rb.velocity = new Vector2(rb.velocity.x, JumpForce);
                    JumpTimeCounter -= Time.deltaTime;
            }
        }

        if (Input.GetKeyUp(KeyCode.Space) || Input.GetMouseButtonDown(0))
        {
            JumpTimeCounter = 0;
        }

        if (IsGrounded)
            JumpTimeCounter = JumpTime;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Lava" || collision.tag == "stone")
        {
            if (collision.tag == "stone" && isShield == true)
            {
                isShield = false;
                Shield.SetActive(false);
            }
            else
            {
                Time.timeScale = 0;
                DieScreen.SetActive(true);
            }
            
            
        }

        if(collision.tag == "Shield" )
        {
            collision.gameObject.SetActive(false);
            Shield.SetActive(true);
            isShield = true;
        }
            
    }
}
