﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformGenerator : MonoBehaviour
{

    public GameObject platform;
    public Transform generationPoint;
    float distanceBetween;
    public GameObject PlayerController;

    float distanceMin = 2;
    float distanceMax = 4;

    public GameObject[] platforms;
    public GameObject[] stones;
    int platformSelector;
    int stonesSelector;
    public GameObject Empty;
    float[] platformsWidth;
    private Vector2 posStones;
    public Transform maxHightPoint;
    public float maxHightChange;
    private float HightChange;
    private float FixPosMidY;

    public GameObject stars;
    private Vector3 posStars;

    public GameObject ShieldPref;



    void Start()
    {
        platformsWidth = new float[platforms.Length];
        
        
        
        for (int i = 0; i < platforms.Length; i++)
        {
            platformsWidth[i] = platforms[i].GetComponent<BoxCollider2D>().size.x;
        }
        FixPosMidY = transform.position.y;
        
        posStones = new Vector3(9.9f, 8.7f, 0f);

    }

    void Update()
    {
        

        Debug.Log(transform.position.x);


        if(transform.position.x < generationPoint.position.x)
        {

            distanceBetween = Random.Range(distanceMin, distanceMax);


            platformSelector = Random.Range(0, platforms.Length);
            stonesSelector = Random.Range(0, stones.Length);

            HightChange = FixPosMidY + Random.Range(maxHightChange, -maxHightChange);

            transform.position = new Vector3(transform.position.x + platformsWidth[platformSelector] + distanceBetween, HightChange, transform.position.z);

            if (Random.Range(1,5) == 1)
            {
                Instantiate(stones[stonesSelector], posStones , Empty.transform.rotation);
            }

            posStars = new Vector2(transform.position.x + 3f, transform.position.y + 2f);

            if(Random.Range(1,5) == 1)
                Instantiate(stars, posStars, transform.rotation);

            if (Random.Range(1, 50) == 5)
                Instantiate(ShieldPref, posStars, transform.rotation);

            posStones = new Vector3(PlayerController.transform.position.x + 17f, Empty.transform.position.y, transform.position.z);

            Instantiate(platforms[platformSelector], transform.position, transform.rotation);
        }
    }
}
