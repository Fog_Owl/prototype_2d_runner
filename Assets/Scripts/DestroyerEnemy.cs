﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyerEnemy : MonoBehaviour
{

    public GameObject pointDestroyer;
    void Start()
    {
        pointDestroyer = GameObject.Find("DestroyPoint");
    }

    void Update()
    {
        if (transform.position.x < pointDestroyer.transform.position.x)
            DestroyEn();
            
            
    }
    public void DestroyEn()
    {
        Destroy(gameObject);
    }
}
