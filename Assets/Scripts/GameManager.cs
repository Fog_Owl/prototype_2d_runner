﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    //public GameObject EndWindow;
    // public GameObject AllGame;
    /*public void EndGame()
    {
        Debug.Log("win");
    }    */

    
    public PlatformGenerator platformGenerator;
    public GameObject platformObj;
    private Vector3 platformStartPoint;
    public GameObject Startpos;

    public PlayerController thePlayer;
    private Vector3 playerStartPoint;

    private DestroyerEnemy[] enemy;
    public ScoreManagerScript scoreManager;

    public GameObject PauseWindow;
    

    private void Start()
    {
        platformStartPoint = platformGenerator.transform.position;
        playerStartPoint = thePlayer.transform.position;
    }


    private void Update()
    {
        
    }
    public void RestartGame()
    {
        thePlayer.DieScreen.SetActive(false);
        Time.timeScale = 1;
        StartCoroutine("RestartGameCo");
    }

    public void ExitInMainMenu()
    {
        SceneManager.LoadScene("Main_menu");
    }
    public IEnumerator RestartGameCo ()
    {

        thePlayer.gameObject.SetActive(false);
        
        yield return new WaitForSeconds(0.1f);
        enemy = FindObjectsOfType<DestroyerEnemy>();
        for (int i = 0; i < enemy.Length; i++)
        {
            enemy[i].DestroyEn();
        }
        thePlayer.StaticMoveSpeed = 5f;
        scoreManager.scoreCount = 0;
        thePlayer.transform.position = playerStartPoint;
        platformGenerator.transform.position = platformStartPoint;
        thePlayer.gameObject.SetActive(true);
        Debug.Log(platformGenerator.transform.position + "pos");

    }

    public void GoGame()
    {
        Time.timeScale = 1;
        PauseWindow.SetActive(false);

    }

    public void StopGame()
    {
        Time.timeScale = 0;
        PauseWindow.SetActive(true);
    }
}
