﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Main_menu : MonoBehaviour
{

    
    public void PlayGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Version_1.0.4");
    }

    public void ExitGame()
    {
        Application.Quit();
    }


}
