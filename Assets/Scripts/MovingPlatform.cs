﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    float speed = 2f;

    public bool MovingRight = true;

    void Update()
    {
        if (transform.position.y > 2f)
            MovingRight = false;
        else if (transform.position.y < -2f)
            MovingRight = true;

        if (MovingRight)
            transform.position = new Vector2(transform.position.x, transform.position.y + speed * Time.deltaTime);
        else
            transform.position = new Vector2(transform.position.x, transform.position.y - speed * Time.deltaTime);
    }
}
